# TODO App

A wonderful TODO list application.

### Prerequisites

/!\ Application tested with Android 11.0 version /!\

### Installation

1. Clone the repository.
2. Compile the source code.
3. Enjoy your brand new application !

### Usages

- You can create multiple categories with each independent tasks.
- Each task can have a programmable deadline.
- Each task can have a reminder alarm programmable.
- Each task can have an independent background color.

*Illustrations below show the possibilities offered by the app:*
- Create a new category:

![alt text](category_illustration.PNG)

- Create a new task:

![alt text](task_illustration.PNG)

- Change background color:

![alt text](color_illustration.PNG)

- Choose your date with a calendar:

![alt text](calendar_illustration.PNG)

- Set an alarm reminder:

![alt text](alarm_illustration.PNG)

### Running the tests

***The app is not yet entirely tested***  
*Tests are runnable in app/java/com/titan/todoapp*

### Versioning

Version 2.0

### Authors

- [Overlorde](https://gitlab.com/Overlorde)

### License

This project is under MIT License.

### Acknowledgments

Created with Java & Android love
