package com.titan.model;

import android.graphics.Color;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Task class
 */
public class Task {
    /**
     * The title of the task
     */
    private String title;
    /**
     * The description of the task
     */
    private String description;
    /**
     * The creation date of the task
     */
    private final String date;
    /**
     * The deadline of the task
     */
    private String deadline;
    /**
     * The color of the task cardView
     */
    private int color;
    /**
     * The status of the alarm
     */
    private Alarm alarm;
    /**
     * Id of the task necessary to create or cancel its alarm
     */
    private final int id;

    /**
     * Constructor of the Task
     *
     * @param name        the name of the task
     * @param description the description of the task
     * @param date        the creation date of the task
     */
    public Task(String name, String description, String date) {
        this.id = ThreadLocalRandom.current().nextInt();
        this.title = name;
        this.description = description;
        this.date = date;
        this.color = Color.rgb(255, 165, 0);
        this.alarm = Alarm.NONE;
    }

    /**
     * Gets the title of the task
     *
     * @return the title of the task
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title of the task
     *
     * @param title the title of the task
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Gets the description of the task
     *
     * @return the description of the task
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description of the task
     *
     * @param description the description of the task
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the creation date of the task
     *
     * @return the creation date of the task
     */
    public String getDate() {
        return date;
    }

    /**
     * Gets the deadline of the task
     *
     * @return the deadline of the task
     */
    public String getDeadline() {
        return deadline;
    }

    /**
     * Sets the deadline of the task
     *
     * @param deadline the deadline of the task
     */
    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    /**
     * Gets the color of the task cardView
     *
     * @return the color of the task cardView
     */
    public int getColor() {
        return color;
    }

    /**
     * Sets the color of the task cardView
     *
     * @param color the color of the task cardView
     */
    public void setColor(int color) {
        this.color = color;
    }

    /**
     * Gets the level of alarm
     *
     * @return the alarm level
     */
    public Alarm getAlarm() {
        return alarm;
    }

    /**
     * Sets the level of alarm
     *
     * @param alarm the new alarm level
     */
    public void setAlarm(Alarm alarm) {
        this.alarm = alarm;
    }

    /**
     * Gets the id of the task
     *
     * @return the task id
     */
    public int getId() {
        return id;
    }
}
