package com.titan.model;

/**
 * Enum of the alarm frequency values
 */
public enum Alarm {
    NONE,
    EVERY_DAY,
    EVERY_MONTH,
    EVERY_YEAR
}
