package com.titan.model;

import android.graphics.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * Category class
 */
public class Category {

    /**
     * The name of the category
     */
    private String name;
    /**
     * The color of the category cardView
     */
    private int color;
    /**
     * The task list of the category
     */
    private List<Task> tasks;

    /**
     * Constructor of Category
     */
    public Category() {
        this.tasks = new ArrayList<>();
        this.color = Color.rgb(255, 165, 0);

    }

    /**
     * Gets the name of the category
     *
     * @return the category name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the category name
     *
     * @param name the name of the category
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the tasks of the category
     *
     * @return the task list of the category
     */
    public List<Task> getTasks() {
        return tasks;
    }

    /**
     * Sets the task list of the category
     *
     * @param tasks the task list
     */
    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    /**
     * Gets the color of the category cardView
     *
     * @return the cardView category color
     */
    public int getColor() {
        return color;
    }

    /**
     * Sets the color of the cardView category
     *
     * @param color the color of the category cardView
     */
    public void setColor(int color) {
        this.color = color;
    }
}
