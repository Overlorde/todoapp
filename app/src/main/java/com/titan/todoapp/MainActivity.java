package com.titan.todoapp;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.SearchView;
import androidx.core.app.NotificationCompat;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.titan.DAO.CategoryList;
import com.titan.model.Alarm;
import com.titan.model.Category;
import com.titan.model.Task;
import com.titan.utils.CategoryAdapter;
import com.titan.utils.OnCategoryClickListener;
import com.titan.utils.TaskAdapter;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity implements OnCategoryClickListener {
    /**
     * The android channel to display the notification
     */
    private static final String CHANNEL_ID = "channel1";
    /**
     * The list of categories
     */
    private static CategoryList categoryList;
    /**
     * The current position of the category in regards of the user's click
     */
    private int currentPositionCategory;
    /**
     * The current position of the task in regards of the user's click
     */
    private int currentPositionTask;
    /**
     * The current text on the cardView
     */
    private EditText currentText;
    /**
     * A boolean to know if the selected title is from category or task
     */
    private boolean isTitleTask;
    /**
     * AdapterCategory
     */
    private RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> adapterCategory;
    /**
     * TaskAdapter
     */
    private RecyclerView.Adapter<TaskAdapter.CategoryViewHolder> adapterTask;
    /**
     * Used to know if the cardView is the one from category or task
     */
    private boolean isCategory;
    /**
     * Used to know if user clicked on popup menu
     */
    private boolean isMenuPopClick = false;
    /**
     * Shared alarm button
     */
    private ImageView buttonAlarm;

    /**
     * Instance method when starting the application
     *
     * @param savedInstanceState A savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createNotificationChannel();
        setContentView(R.layout.display_categorylist);
        categoryList = new CategoryList();
        if (getTasksFromSharedPrefs(this) != null) {
            categoryList = getTasksFromSharedPrefs(this);
        }
        if (categoryList.getCategoryList().size() == 0) {
            List<Category> categories = new ArrayList<>();
            Category category = new Category();
            category.setName("Catégorie par défaut");
            List<Task> tasks = new ArrayList<>();
            DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, Locale.FRENCH);
            tasks.add(new Task("", "", dateFormat.format(new Date())));
            category.setTasks(tasks);
            categories.add(category);
            categoryList.setCategoryList(categories);
            saveTasksToSharedPrefs(this);
        }
        updateCategoryList();

    }

    /**
     * Updates the category cardViews on user's actions
     */
    public void updateCategoryList() {
        RecyclerView recyclerViewCategory = findViewById(R.id.display_categorylist);
        recyclerViewCategory.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerViewCategory.setLayoutManager(layoutManager);
        adapterCategory = new CategoryAdapter(categoryList.getCategoryList(), this);
        recyclerViewCategory.setAdapter(adapterCategory);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(500);
        itemAnimator.setRemoveDuration(500);
        recyclerViewCategory.setItemAnimator(itemAnimator);
        isCategory = true;
        SearchView searchView = recyclerViewCategory.getRootView().findViewById(R.id.searchViewCategory);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                int categoryFoundPos = callSearch(query);
                if (categoryFoundPos != -1) {
                    Category tempCategory = categoryList.getCategoryList().get(categoryFoundPos);
                    categoryList.getCategoryList().remove(categoryFoundPos);
                    categoryList.getCategoryList().add(0, tempCategory);
                    adapterCategory.notifyDataSetChanged();
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                int categoryFoundPos = callSearch(newText);
                if (categoryFoundPos != -1) {
                    Category tempCategory = categoryList.getCategoryList().get(categoryFoundPos);
                    categoryList.getCategoryList().remove(categoryFoundPos);
                    categoryList.getCategoryList().add(0, tempCategory);
                    adapterCategory.notifyDataSetChanged();
                }
                return true;
            }

            public int callSearch(String query) {
                for (Category category : categoryList.getCategoryList()) {
                    Pattern pattern = Pattern.compile(query);
                    if (category.getName() != null) {
                        Matcher matcher = pattern.matcher(category.getName().toLowerCase());
                        if (matcher.lookingAt()) {
                            return categoryList.getCategoryList().indexOf(category);
                        }
                    }
                }
                return -1;
            }

        });
    }

    /**
     * Updates task cardView on user's actions
     *
     * @param categoryPosition the category position selected by the user
     */
    public void updateTaskList(int categoryPosition) {
        RecyclerView recyclerViewTask = findViewById(R.id.display_listTask);
        recyclerViewTask.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerViewTask.setLayoutManager(layoutManager);
        adapterTask = new TaskAdapter(categoryList.getCategoryList().get(categoryPosition).getTasks(), this);
        recyclerViewTask.setAdapter(adapterTask);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(500);
        itemAnimator.setRemoveDuration(500);
        recyclerViewTask.setItemAnimator(itemAnimator);
        isCategory = false;
        SearchView searchView = recyclerViewTask.getRootView().findViewById(R.id.searchViewTask);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                int taskFoundPos = callSearch(query);
                if (taskFoundPos != -1) {
                    Task tempTask = categoryList.getCategoryList().get(categoryPosition).getTasks().get(taskFoundPos);
                    categoryList.getCategoryList().get(categoryPosition).getTasks().remove(taskFoundPos);
                    categoryList.getCategoryList().get(categoryPosition).getTasks().add(0, tempTask);
                    adapterTask.notifyDataSetChanged();
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                int taskFoundPos = callSearch(newText);
                if (taskFoundPos != -1) {
                    Task tempTask = categoryList.getCategoryList().get(categoryPosition).getTasks().get(taskFoundPos);
                    categoryList.getCategoryList().get(categoryPosition).getTasks().remove(taskFoundPos);
                    categoryList.getCategoryList().get(categoryPosition).getTasks().add(0, tempTask);
                    adapterTask.notifyDataSetChanged();
                }
                return true;
            }

            public int callSearch(String query) {
                for (Task task : categoryList.getCategoryList().get(categoryPosition).getTasks()) {
                    Pattern pattern = Pattern.compile(query);
                    if (task.getTitle() != null) {
                        Matcher matcher = pattern.matcher(task.getTitle().toLowerCase());
                        if (matcher.lookingAt()) {
                            return categoryList.getCategoryList().get(categoryPosition).getTasks().indexOf(task);
                        }
                    }
                }
                return -1;
            }
        });
    }

    /**
     * Creates a task
     *
     * @param view the task view
     */
    public void createTask(View view) {
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, Locale.FRENCH);
        String date = dateFormat.format(new Date());
        categoryList.getCategoryList().get(currentPositionCategory).getTasks().add(new Task("", "", date));
        adapterTask.notifyDataSetChanged();
        saveTasksToSharedPrefs(this);
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        CharSequence name = getString(R.string.channel_name);
        String description = getString(R.string.channel_description);
        int importance = NotificationManager.IMPORTANCE_DEFAULT;
        NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
        channel.setDescription(description);
        // Register the channel with the system; you can't change the importance
        // or other notification behaviors after this
        NotificationManager notificationManager = getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannel(channel);
    }

    /**
     * Handles the back input of the user's keyboard
     *
     * @param view the view
     */
    public void backToMenu(View view) {
        saveTasksToSharedPrefs(this);
        setContentView(R.layout.display_categorylist);
        updateCategoryList();
    }

    /**
     * Saves the existing tasks
     *
     * @param context the context
     */
    public void saveTasksToSharedPrefs(Context context) {
        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(categoryList);
        prefsEditor.putString("currentCategories", json);
        prefsEditor.apply();
    }

    /**
     * Gets the tasks from the secret settings
     *
     * @param context the context
     * @return the list of categories
     */
    public CategoryList getTasksFromSharedPrefs(Context context) {
        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        Gson gson = new Gson();
        String json = appSharedPrefs.getString("currentCategories", "");
        return gson.fromJson(json, CategoryList.class);
    }

    /**
     * Handles the key up from the user's keyboard
     *
     * @param keyCode the KeyCode
     * @param event   the event
     * @return a boolean
     */
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK || event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            saveText();
            currentText.setCursorVisible(false);
        }
        return true;
    }

    /**
     * Handles the creation of a category
     *
     * @param view the category view
     */
    public void createCategory(View view) {
        categoryList.getCategoryList().add(new Category());
        saveTasksToSharedPrefs(this);
        adapterCategory.notifyItemInserted(categoryList.getCategoryList().size());
    }

    /**
     * Handles when the user click on a category
     *
     * @param categoryPosition the cardView position
     */
    @Override
    public void OnClickCategory(int categoryPosition) {
        saveTasksToSharedPrefs(this);
        setContentView(R.layout.display_tasklist);
        currentPositionCategory = categoryPosition;
        updateTaskList(categoryPosition);
    }

    /**
     * Handles when the user long click on a category
     *
     * @param currentId the id of the selected cardView
     */
    @Override
    public void OnLongClickCategory(int currentId) {
        categoryList.getCategoryList().remove(currentId);
        saveTasksToSharedPrefs(this);
        adapterCategory.notifyItemRangeChanged(currentId, categoryList.getCategoryList().size());
    }

    /**
     * Handles the long click on a task
     *
     * @param adapterPosition the selected task position
     */
    @Override
    public void OnLongClickTask(int adapterPosition) {
        categoryList.getCategoryList().get(currentPositionCategory).getTasks().remove(adapterPosition);
        saveTasksToSharedPrefs(this);
    }

    /**
     * Handles a click on a task
     *
     * @param adapterPosition the selected task position
     * @param editText        the description of the cardView task
     * @param isTitleTask     the boolean of the selected cardView
     */
    @Override
    public void OnClickTask(int adapterPosition, EditText editText, boolean isTitleTask) {
        this.isTitleTask = isTitleTask;
        currentPositionTask = adapterPosition;
        if (currentPositionTask != -1) {
            currentText = editText;
            saveText();
        }
    }

    /**
     * Saves the text when the user stopped editing
     */
    public void saveText() {
        if (categoryList.getCategoryList().size() != 0) {
            TaskAdapter taskAdapter = new TaskAdapter(categoryList.getCategoryList().get(currentPositionCategory).getTasks(), this);
            if (!isCategory) {
                if (!isTitleTask) {
                    categoryList.getCategoryList().get(currentPositionCategory).getTasks().get(currentPositionTask).setDescription(currentText.getText().toString());
                } else {
                    categoryList.getCategoryList().get(currentPositionCategory).getTasks().get(currentPositionTask).setTitle(currentText.getText().toString());
                }
            } else {
                categoryList.getCategoryList().get(currentPositionCategory).setName(currentText.getText().toString());
            }
            taskAdapter.notifyDataSetChanged();
            saveTasksToSharedPrefs(this);
        }
    }

    /**
     * Handles when the user click on a task
     *
     * @param adapterPosition the selected task position
     */
    @Override
    public void OnClickTask(int adapterPosition) {
        currentPositionTask = adapterPosition;
    }

    /**
     * Handles when the user click on a category title
     *
     * @param adapterPosition the cardView position
     * @param editText        the title of the category
     */
    @Override
    public void OnClickTitleCategory(int adapterPosition, EditText editText) {
        if (adapterPosition != -1) {
            currentPositionCategory = adapterPosition;
        }
        this.currentText = editText;
        saveText();
    }

    /**
     * Handles when the user click on the dots menu belonging to category
     *
     * @param adapterPosition the cardView position
     * @param view            the view
     */
    @Override
    public void OnClickToggleMenuCategory(int adapterPosition, View view) {
        if (adapterPosition != -1) {
            currentPositionCategory = adapterPosition;
        }
        showPopupColor(view);
    }

    /**
     * Handles the click on the calendar ImageView
     *
     * @param adapterPosition the cardView position
     * @param year            the year
     * @param month           the month
     * @param day             the day
     */
    @Override
    public void OnClickTaskShowCalendar(int adapterPosition, int year, int month, int day) {
        currentPositionTask = adapterPosition;
        Calendar date = Calendar.getInstance();
        date.set(Calendar.YEAR, year);
        date.set(Calendar.MONTH, month);
        date.set(Calendar.DAY_OF_MONTH, day);
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, Locale.FRENCH);
        categoryList.getCategoryList().get(currentPositionCategory).getTasks().get(currentPositionTask).setDeadline(dateFormat.format(date.getTime()) + "\n" + "(" + "REMAIN_DAY" + " jour(s) restant(s)" + ")");
        try {
            long remainTime = date.getTime().getTime() - Objects.requireNonNull(dateFormat.parse(dateFormat.format(new Date()))).getTime();
            categoryList.getCategoryList()
                    .get(currentPositionCategory)
                    .getTasks().get(currentPositionTask)
                    .setDeadline(categoryList.getCategoryList().get(currentPositionCategory).getTasks()
                            .get(currentPositionTask).getDeadline().replace("REMAIN_DAY", String.valueOf(TimeUnit.MILLISECONDS.toDays(remainTime) + 1)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        saveTasksToSharedPrefs(this);
        adapterTask.notifyDataSetChanged();
    }

    /**
     * Shows the submenu from the dots button of the category
     *
     * @param view the View
     */
    @SuppressLint("RestrictedApi")
    public void showPopupColor(View view) {
        MenuBuilder menuBuilder = new MenuBuilder(this);
        MenuInflater inflater = new MenuInflater(this);
        inflater.inflate(R.menu.popup_color, menuBuilder);
        MenuPopupHelper optionsMenu = new MenuPopupHelper(this, menuBuilder, view);
        optionsMenu.setForceShowIcon(true);
        optionsMenu.show();
    }

    /**
     * Shows the submenu from the dots button of the task
     *
     * @param view the View
     */
    @SuppressLint("RestrictedApi")
    public void showPopupAlarm(View view) {
        MenuBuilder menuBuilder = new MenuBuilder(this);
        MenuInflater inflater = new MenuInflater(this);
        inflater.inflate(R.menu.layout_popup_menu_alarm, menuBuilder);
        MenuPopupHelper optionsMenu = new MenuPopupHelper(this, menuBuilder, view);
        optionsMenu.setForceShowIcon(true);
        optionsMenu.setOnDismissListener(() -> {
            buttonAlarm = view.findViewById(R.id.buttonAlarm);
            if (!isMenuPopClick) {
                buttonAlarm.setImageResource(R.drawable.ic_baseline_add_alert_24);
            }
            isMenuPopClick = false;
        });
        optionsMenu.show();
    }

    /**
     * Handles when the user click on the dots menu belonging to task
     *
     * @param adapterPosition the cardView position
     * @param view            the view
     */
    @Override
    public void OnClickToggleMenuTask(int adapterPosition, View view) {
        if (adapterPosition != -1) {
            currentPositionTask = adapterPosition;
        }
        showPopupColor(view);
    }


    /**
     * Handles the user's click on the bell notification icon
     *
     * @param adapterPosition the cardView position
     * @param view            the clock view
     */
    public void OnClickAlarmToTask(int adapterPosition, View view) {
        ImageView buttonAlarm = view.findViewById(R.id.buttonAlarm);
        if (adapterPosition != -1) {
            currentPositionTask = adapterPosition;
        }
        if (!categoryList.getCategoryList().get(currentPositionCategory).getTasks().get(currentPositionTask).getAlarm().equals(Alarm.NONE)) {
            categoryList.getCategoryList().get(currentPositionCategory).getTasks().get(currentPositionTask).setAlarm(Alarm.NONE);
            buttonAlarm.setImageResource(R.drawable.ic_baseline_add_alert_24);
            cancelAlarm(categoryList.getCategoryList().get(currentPositionCategory).getTasks().get(currentPositionTask).getId());
        } else {
            showPopupAlarm(view);
            buttonAlarm.setImageResource(R.drawable.ic_baseline_notifications_active_32);
        }
    }

    /**
     * Handles the user's click
     *
     * @param item the item selected by the user
     */
    public void setAlarmToTask(MenuItem item) {
        // Get Current Time
        isMenuPopClick = true;
        final Calendar calendar = Calendar.getInstance();
        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                (view1, hourOfDay, minuteOfDay) -> {
                    Alarm alarm = Alarm.NONE;
                    if (item.getTitle().toString().equals("Tout les jours")) {
                        alarm = Alarm.EVERY_DAY;
                    } else if (item.getTitle().toString().equals("Tout les mois")) {
                        alarm = Alarm.EVERY_MONTH;
                    } else if (item.getTitle().toString().equals("Tout les ans")) {
                        alarm = Alarm.EVERY_YEAR;
                    }
                    categoryList.getCategoryList().get(currentPositionCategory).getTasks().get(currentPositionTask).setAlarm(alarm);
                    scheduleNotification(getNotification(), hourOfDay, minuteOfDay, alarm, categoryList.getCategoryList().get(currentPositionCategory).getTasks().get(currentPositionTask).getId());
                }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
        timePickerDialog.setOnCancelListener(dialog -> buttonAlarm.setImageResource(R.drawable.ic_baseline_add_alert_24));
        timePickerDialog.show();
        saveTasksToSharedPrefs(this);
    }

    /**
     * Schedules the notification alarm
     *
     * @param notification the notification
     * @param hours        the hour to set the alarm
     * @param minutes      the minutes to set the alarm
     * @param alarm        the alarm
     * @param id           the id of the task to link with the alarm
     */
    private void scheduleNotification(Notification notification, int hours, int minutes, Alarm alarm, int id) {
        Intent notificationIntent = new Intent(this, AlarmReceiver.class);
        notificationIntent.putExtra(AlarmReceiver.NOTIFICATION_ID, 1);
        notificationIntent.putExtra(AlarmReceiver.NOTIFICATION, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, id, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        ComponentName receiver = new ComponentName(this, AlarmReceiver.class);
        PackageManager pm = getApplicationContext().getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, hours);
        calendar.set(Calendar.MINUTE, minutes);

        if (Calendar.getInstance().after(calendar)) {
            // Move to tomorrow
            calendar.add(Calendar.DATE, 1);
        }

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        if (alarm.equals(Alarm.EVERY_DAY)) {
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent); //Repeat every 24 hours
        } else if (alarm.equals(Alarm.EVERY_MONTH)) {
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 30 * AlarmManager.INTERVAL_DAY, pendingIntent); //Repeat every 30 days ~ month
        } else if (alarm.equals(Alarm.EVERY_YEAR)) {
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 365 * AlarmManager.INTERVAL_DAY, pendingIntent); //Repeat every year
        } else {
            throw new IllegalArgumentException("Alarm value is not defined in Enum.");
        }
    }

    /**
     * Builds the notification
     *
     * @return the notification
     */
    private Notification getNotification() {
        return new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_baseline_notification_important_33)
                .setContentTitle("Rappel de votre tâche")
                .setContentText("Cliquez sur la notification pour plus de détails.")
                .setStyle(new NotificationCompat.BigTextStyle().bigText("Cliquez sur la notification pour plus de détails."))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT).build();
    }

    /**
     * Allows to cancel the alarm notification of a task
     *
     * @param id the task id
     */
    public void cancelAlarm(int id) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent myIntent = new Intent(getApplicationContext(), AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                getApplicationContext(), id, myIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        ComponentName receiver = new ComponentName(this, AlarmReceiver.class);
        PackageManager pm = this.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
        alarmManager.cancel(pendingIntent);
    }

    /**
     * Handles the change of the background color of the task cardView
     *
     * @param item the selected color item
     */
    public void changeTaskColorBackground(MenuItem item) {
        int color;
        switch (item.getTitle().toString()) {
            case "rouge":
                color = Color.rgb(178, 34, 34);
                break;
            case "gris":
                color = Color.rgb(128, 128, 128);
                break;
            case "vert":
                color = Color.rgb(0, 128, 0);
                break;
            case "bleu":
                color = Color.rgb(58, 142, 186);
                break;
            case "jaune":
                color = Color.rgb(252, 226, 5);
                break;
            default:
                color = Color.rgb(255, 165, 0);
                break;
        }
        RecyclerView recyclerViewTask = findViewById(R.id.display_listTask);
        if (recyclerViewTask != null) { // check if display is on category or task
            categoryList.getCategoryList().get(currentPositionCategory).getTasks().get(currentPositionTask).setColor(color);
            adapterTask.notifyDataSetChanged();
        } else {
            categoryList.getCategoryList().get(currentPositionCategory).setColor(color);
            adapterCategory.notifyDataSetChanged();
        }
        saveTasksToSharedPrefs(this);
    }
}
