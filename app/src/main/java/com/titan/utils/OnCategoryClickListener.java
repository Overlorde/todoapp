package com.titan.utils;

import android.view.View;
import android.widget.EditText;

/**
 * OnCategoryClickListener class
 */
public interface OnCategoryClickListener {
    /**
     * Handles the event when the user click on the cardView category
     *
     * @param categoryPosition the cardView position
     */
    void OnClickCategory(int categoryPosition);

    /**
     * Handles the event when the user long click on the cardView category
     *
     * @param currentId the id of the selected cardView
     */
    void OnLongClickCategory(int currentId);

    /**
     * Handles the event when the user long click on the cardView task
     *
     * @param adapterPosition the selected task position
     */
    void OnLongClickTask(int adapterPosition);

    /**
     * Handles the event when the user click on the cardView task
     *
     * @param adapterPosition the selected task position
     */
    void OnClickTask(int adapterPosition);

    /**
     * Handles the event when the user click on the cardView task
     *
     * @param adapterPosition the selected task position
     * @param editText        the description of the cardView task
     * @param isTitleTask     the boolean of the selected cardView
     */
    void OnClickTask(int adapterPosition, EditText editText, boolean isTitleTask);

    /**
     * Handles the user click on the category title
     *
     * @param adapterPosition the cardView position
     * @param editText        the title of the task
     */
    void OnClickTitleCategory(int adapterPosition, EditText editText);

    /**
     * Handles the user click on calendar ImageView
     *
     * @param adapterPosition the cardView position
     * @param year            the year
     * @param month           the month
     * @param day             the day
     */
    void OnClickTaskShowCalendar(int adapterPosition, int year, int month, int day);

    /**
     * Handles the user click on the dots of the category cardView
     *
     * @param adapterPosition the cardView position
     * @param view            the view
     */
    void OnClickToggleMenuCategory(int adapterPosition, View view);

    /**
     * Handles the user click on the dots of the task cardView
     *
     * @param adapterPosition the cardView position
     * @param view            the view
     */
    void OnClickToggleMenuTask(int adapterPosition, View view);

    /**
     * Handles the action when the user click on the alarm ImageView
     *
     * @param adapterPosition the cardView position
     * @param view            the clock view
     */
    void OnClickAlarmToTask(int adapterPosition, View view);

}
