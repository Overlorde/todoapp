package com.titan.utils;

import android.annotation.SuppressLint;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.titan.model.Category;
import com.titan.todoapp.R;

import java.util.List;

/**
 * CategoryAdapter class
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {
    /**
     * The list of categories
     */
    private final List<Category> categoryList;
    /**
     * Listener of the category cardView
     */
    private final OnCategoryClickListener onCategoryClickListener;

    /**
     * CategoryViewHolder class
     */
    public static class CategoryViewHolder extends RecyclerView.ViewHolder {
        /**
         * The view
         */
        View view;
        /**
         * Number of the category
         */
        TextView categoryNumber;
        /**
         * Title of the category
         */
        EditText categoryTitle;
        /**
         * The imageView for the color menu
         */
        ImageView colorMenu;
        /**
         * The RelativeLayout
         */
        RelativeLayout relativeLayout;

        /**
         * CategoryViewHolder constructor
         *
         * @param view the view
         */
        CategoryViewHolder(View view) {
            super(view);
            this.view = view;
            categoryNumber = view.findViewById(R.id.category_number);
            categoryTitle = view.findViewById(R.id.title_category);
            relativeLayout = view.findViewById(R.id.relative_layout);
            colorMenu = view.findViewById(R.id.menuColorCategory);
        }
    }

    /**
     * Provide a suitable constructor (depends on the kind of dataset)
     *
     * @param categoryList            the list of categories
     * @param onCategoryClickListener the OnCategoryClickListener
     */
    public CategoryAdapter(List<Category> categoryList, OnCategoryClickListener onCategoryClickListener) {
        this.categoryList = categoryList;
        this.onCategoryClickListener = onCategoryClickListener;
    }

    /**
     * Override onCreateViewHolder which creates new views (invoked by the layout manager)
     *
     * @param parent   the parent view
     * @param viewType the viewType
     * @return CategoryViewHolder
     */
    @NonNull
    @Override
    public CategoryAdapter.CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category, parent, false);
        return new CategoryViewHolder(view);
    }

    /**
     * Override onBindViewHolder which replaces the contents of a view (invoked by the layout manager)
     *
     * @param holder   the CategoryViewHolder
     * @param position the position of the category CardView
     */
    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.R)
    @Override
    public void onBindViewHolder(@NonNull final CategoryViewHolder holder, final int position) {

        holder.categoryTitle.setText(categoryList.get(holder.getBindingAdapterPosition()).getName());
        String category_number = "Catégorie n°".concat(String.valueOf(holder.getBindingAdapterPosition() + 1));
        holder.categoryNumber.setText(category_number);
        holder.relativeLayout.setBackgroundColor(categoryList.get(position).getColor());
        holder.view.setOnLongClickListener(v -> {
            onCategoryClickListener.OnLongClickCategory(holder.getBindingAdapterPosition());
            notifyItemRemoved(holder.getBindingAdapterPosition());
            return true;
        });

        holder.view.setOnClickListener(v -> onCategoryClickListener.OnClickCategory(holder.getBindingAdapterPosition()));
        holder.colorMenu.setOnClickListener(v -> onCategoryClickListener.OnClickToggleMenuCategory(holder.getBindingAdapterPosition(), holder.colorMenu));
        holder.categoryTitle.setOnFocusChangeListener((v, hasFocus) -> onCategoryClickListener.OnClickTitleCategory(holder.getBindingAdapterPosition(), holder.categoryTitle));
    }

    /**
     * Override getItemCount
     *
     * @return the number of categories
     */
    @Override
    public int getItemCount() {
        if (categoryList != null) {
            return categoryList.size();
        } else {
            return 0;
        }
    }
}
