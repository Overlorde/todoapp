package com.titan.utils;

import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.titan.model.Alarm;
import com.titan.model.Task;
import com.titan.todoapp.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

/**
 * TaskAdapter class
 */
public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.CategoryViewHolder> {
    /**
     * The list of tasks
     */
    private final List<Task> tasks;
    /**
     * The OnCategoryClick listener
     */
    private final OnCategoryClickListener onCategoryClickListener;

    /**
     * CategoryViewHolder class
     */
    public static class CategoryViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        View view;
        EditText title;
        TextView creationDate;
        TextView deadlineDate;
        EditText description;
        RelativeLayout relativeLayout;
        ImageView buttonDate;
        ImageView buttonAlarm;
        ImageView colorMenu;
        CalendarView calendarView;
        TimePicker timePicker;

        /**
         * CategoryViewHolder constructor
         *
         * @param view the task cardView
         */
        CategoryViewHolder(View view) {
            super(view);
            this.view = view;
            this.title = view.findViewById(R.id.titleTask);
            this.description = view.findViewById(R.id.descriptionTask);
            this.creationDate = view.findViewById(R.id.dateTask);
            this.relativeLayout = view.findViewById(R.id.relative_layout);
            this.deadlineDate = view.findViewById(R.id.calendarText);
            this.buttonDate = view.findViewById(R.id.buttonDate);
            this.calendarView = view.findViewById(R.id.calendarView);
            this.colorMenu = view.findViewById(R.id.menuColorTask);
            this.timePicker = view.findViewById(R.id.timePicker);
            this.buttonAlarm = view.findViewById(R.id.buttonAlarm);
            DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, Locale.FRENCH);
            try {
                this.calendarView.setMinDate(Objects.requireNonNull(dateFormat.parse(dateFormat.format(new Date()))).getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            this.calendarView.setVisibility(View.INVISIBLE);
            this.timePicker.setVisibility(View.INVISIBLE);
            this.timePicker.setIs24HourView(true);
        }
    }

    /**
     * TaskAdapter constructor
     *
     * @param tasks                   the list of tasks
     * @param onCategoryClickListener the OnCategoryClickListener
     */
    public TaskAdapter(List<Task> tasks, OnCategoryClickListener onCategoryClickListener) {
        this.tasks = tasks;
        this.onCategoryClickListener = onCategoryClickListener;
    }

    /**
     * Override OnCreateViewHolder
     *
     * @param viewGroup the ViewGroup
     * @param position  the position of the task cardView
     * @return TaskAdapter.CategoryViewHolder
     */
    @NonNull
    @Override
    public TaskAdapter.CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_task, viewGroup, false);
        return new CategoryViewHolder(view);
    }

    /**
     * Override onBindViewHolder
     *
     * @param categoryViewHolder the CategoryViewHolder
     * @param position           the position of the task cardView
     */
    @Override
    public void onBindViewHolder(@NonNull final TaskAdapter.CategoryViewHolder categoryViewHolder, int position) {
        categoryViewHolder.title.setText(tasks.get(position).getTitle());
        categoryViewHolder.description.setText(tasks.get(position).getDescription());
        categoryViewHolder.creationDate.setText(tasks.get(position).getDate());
        categoryViewHolder.deadlineDate.setText(tasks.get(position).getDeadline());
        categoryViewHolder.deadlineDate.setTextColor(Color.WHITE);
        categoryViewHolder.deadlineDate.setTypeface(categoryViewHolder.deadlineDate.getTypeface(), Typeface.BOLD);
        categoryViewHolder.relativeLayout.setBackgroundColor(tasks.get(position).getColor()); //set the color of the background task
        categoryViewHolder.view.setOnLongClickListener(v -> {
            onCategoryClickListener.OnLongClickTask(categoryViewHolder.getBindingAdapterPosition());
            notifyItemRemoved(categoryViewHolder.getBindingAdapterPosition());
            return true;
        });
        categoryViewHolder.view.setOnClickListener(v -> onCategoryClickListener.OnClickTask(categoryViewHolder.getBindingAdapterPosition()));
        categoryViewHolder.description.setOnFocusChangeListener((v, hasFocus) -> onCategoryClickListener.OnClickTask(categoryViewHolder.getBindingAdapterPosition(), categoryViewHolder.description, false));
        categoryViewHolder.title.setOnFocusChangeListener((v, hasFocus) -> onCategoryClickListener.OnClickTask(categoryViewHolder.getBindingAdapterPosition(), categoryViewHolder.title, true));
        categoryViewHolder.buttonDate.setOnClickListener(v -> categoryViewHolder.calendarView.setVisibility(View.VISIBLE));
        categoryViewHolder.buttonAlarm.setOnClickListener(v -> onCategoryClickListener.OnClickAlarmToTask(categoryViewHolder.getBindingAdapterPosition(), categoryViewHolder.buttonAlarm));
        if (!tasks.get(position).getAlarm().equals(Alarm.NONE)) {
            categoryViewHolder.buttonAlarm.setImageResource(R.drawable.ic_baseline_notifications_active_32);
        } else {
            categoryViewHolder.buttonAlarm.setImageResource(R.drawable.ic_baseline_add_alert_24);
        }
        categoryViewHolder.colorMenu.setOnClickListener(v -> onCategoryClickListener.OnClickToggleMenuTask(categoryViewHolder.getBindingAdapterPosition(), categoryViewHolder.colorMenu));
        categoryViewHolder.calendarView.setOnDateChangeListener((view, year, month, dayOfMonth) -> {
            onCategoryClickListener.OnClickTaskShowCalendar(categoryViewHolder.getBindingAdapterPosition(), year, month, dayOfMonth);
            categoryViewHolder.calendarView.setVisibility(View.INVISIBLE);
        });

    }

    /**
     * Override getItemCount
     *
     * @return the number of tasks
     */
    @Override
    public int getItemCount() {
        if (tasks != null) {
            return tasks.size();
        } else {
            return 0;
        }
    }
}