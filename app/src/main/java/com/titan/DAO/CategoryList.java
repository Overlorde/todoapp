package com.titan.DAO;

import com.titan.model.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * CategoryList class
 */
public class CategoryList {
    /**
     * The list of categories
     */
    private List<Category> categoryList;

    /**
     * gets the category list
     *
     * @return the category list
     */
    public List<Category> getCategoryList() {
        return categoryList;
    }

    /**
     * sets the category list
     *
     * @param categoryList the category list
     */
    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    /**
     * Constructor of CategoryList
     */
    public CategoryList() {
        categoryList = new ArrayList<>();
    }

}
