package com.titan.DAO;


import com.titan.model.Category;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

class CategoryListTest {
    private CategoryList categorylist;
    @BeforeEach
    void setUp() {
        categorylist = new CategoryList();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getCategoryList() {
        Assertions.assertEquals(0,categorylist.getCategoryList().size());
    }

    @Test
    void setCategoryList() {
        Category category = new Category();
        ArrayList<Category> categories = new ArrayList<>();
        categories.add(category);
        categorylist.setCategoryList(categories);
        Assertions.assertEquals(1,categorylist.getCategoryList().size());
    }
}