package com.titan.model;

import android.graphics.Color;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TaskTest {

    private Task task;
    @BeforeEach
    void setUp() {
        task = new Task("hello", "world", "05-11-2021");
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getTitle() {
        Assertions.assertEquals("hello",task.getTitle());
    }

    @Test
    void setTitle() {
        task.setTitle("new title");
        Assertions.assertEquals("new title",task.getTitle());
    }

    @Test
    void getDescription() {
        Assertions.assertEquals("world",task.getDescription());
    }

    @Test
    void setDescription() {
        task.setDescription("new description");
        Assertions.assertEquals("new description",task.getDescription());
    }

    @Test
    void getDate() {
        Assertions.assertEquals("05-11-2021",task.getDate());
    }

    @Test
    void getDeadline() {
        Assertions.assertNull(task.getDeadline());
    }

    @Test
    void setDeadline() {
        task.setDeadline("05-12-2021");
        Assertions.assertEquals("05-12-2021", task.getDeadline());
    }

    @Test
    void getColor() {
        Assertions.assertEquals(Color.rgb(255, 165, 0), task.getColor());
    }

    @Test
    void setColor() {
        task.setColor(Color.rgb(255, 255, 255));
        Assertions.assertEquals(Color.rgb(255, 255, 255), task.getColor());
    }

    @Test
    void getAlarm() {
        Assertions.assertEquals(Alarm.NONE, task.getAlarm());
    }

    @Test
    void setAlarm() {
        task.setAlarm(Alarm.EVERY_DAY);
        Assertions.assertEquals(Alarm.EVERY_DAY, task.getAlarm());
    }

    @Test
    void getId() {
        Assertions.assertTrue(task.getId() > 0);
    }
}