package com.titan.model;


import android.graphics.Color;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;

class CategoryTest {
    private Category category;
    @BeforeEach
    void setUp() {
        category = new Category();
        category.setName("testTask");
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getName() {
        Assertions.assertEquals("testTask",category.getName());
    }

    @Test
    void setName() {
        category.setName("newTestTask");
        Assertions.assertEquals("newTestTask", category.getName());
    }

    @Test
    void getTasks() {
        Assertions.assertEquals(new ArrayList<Task>(),category.getTasks());
    }

    @Test
    void setTasks() {
        ArrayList<Task> tasks = new ArrayList<>();
        tasks.add(new Task("hello", "world","05-02-2021"));
        category.setTasks(tasks);
        Assertions.assertEquals("05-02-2021",category.getTasks().get(0).getDate());
        Assertions.assertEquals("hello",category.getTasks().get(0).getTitle());
        Assertions.assertEquals("world",category.getTasks().get(0).getDescription());
    }

    @Test
    void getColor() {
        Assertions.assertEquals(Color.rgb(255, 165, 0), category.getColor());
    }

    @Test
    void setColor() {
        category.setColor(Color.rgb(255, 255, 255));
        Assertions.assertEquals(Color.rgb(255, 255, 255), category.getColor());
    }
}