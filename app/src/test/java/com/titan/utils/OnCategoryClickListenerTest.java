package com.titan.utils;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class OnCategoryClickListenerTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void onClickCategory() {
    }

    @Test
    void onLongClickCategory() {
    }

    @Test
    void onLongClickTask() {
    }

    @Test
    void onClickTask() {
    }

    @Test
    void testOnClickTask() {
    }

    @Test
    void onClickTitleCategory() {
    }

    @Test
    void onClickTaskShowCalendar() {
    }

    @Test
    void onClickToggleMenuCategory() {
    }

    @Test
    void onClickToggleMenuTask() {
    }

    @Test
    void onClickAlarmToTask() {
    }
}