package com.titan.todoapp;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MainActivityTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void onCreate() {
    }

    @Test
    void updateCategoryList() {
    }

    @Test
    void updateTaskList() {
    }

    @Test
    void createTask() {
    }

    @Test
    void backToMenu() {
    }

    @Test
    void saveTasksToSharedPrefs() {
    }

    @Test
    void getTasksFromSharedPrefs() {
    }

    @Test
    void onKeyUp() {
    }

    @Test
    void createCategory() {
    }

    @Test
    void onClickCategory() {
    }

    @Test
    void onLongClickCategory() {
    }

    @Test
    void onLongClickTask() {
    }

    @Test
    void onClickTask() {
    }

    @Test
    void saveText() {
    }

    @Test
    void testOnClickTask() {
    }

    @Test
    void onClickTitleCategory() {
    }

    @Test
    void onClickToggleMenuCategory() {
    }

    @Test
    void onClickTaskShowCalendar() {
    }

    @Test
    void showPopupColor() {
    }

    @Test
    void showPopupAlarm() {
    }

    @Test
    void onClickToggleMenuTask() {
    }

    @Test
    void onClickAlarmToTask() {
    }

    @Test
    void setAlarmToTask() {
    }

    @Test
    void cancelAlarm() {
    }

    @Test
    void changeTaskColorBackground() {
    }
}